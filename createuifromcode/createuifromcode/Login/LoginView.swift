//
//  LoginView.swift
//  createuifromcode
//
//  Created by HA Duyen Hoa on 21.11.18.
//  Copyright © 2018 HDH. All rights reserved.
//

import UIKit
import SnapKit

class LoginView: UIView {
    let loginTextField = UIView.createTextField("enter login")
    let passwordTextField = UIView.createTextField("enter password")
    let loginButton = UIView.createButton("Login")

    let closeButton = UIView.createButton("Close")

    init() {
        super.init(frame: .zero)
        
        setUpView()
        layoutView()
    }

    func setUpView() {
        //additional setup for sub views
        backgroundColor = .white //do not forget that line

        addSubview(loginTextField)
        addSubview(passwordTextField)
        addSubview(loginButton)

        addSubview(closeButton)
    }

    func layoutView() {
        //login textfield's size: 200 x 33 points, 100 points from top
        loginTextField.snp.makeConstraints { maker in
            maker.centerX.equalToSuperview()
            maker.width.equalTo(200)
            maker.height.equalTo(33)
            maker.top.equalToSuperview().offset(100)
        }

        //password textfield's size: 200 x 33 points
        //20 points from bottom of login textfield
        passwordTextField.snp.makeConstraints { maker in
            maker.centerX.equalToSuperview()
            maker.width.equalTo(200)
            maker.height.equalTo(33)
            maker.top.equalTo(loginTextField.snp.bottom).offset(20)
        }

        //loginButton's width: 200 points
        //100 points from top
        loginButton.snp.makeConstraints { maker in
            maker.centerX.equalToSuperview()
            maker.width.equalTo(200)
            maker.top.equalTo(passwordTextField).offset(100)
        }

        closeButton.snp.makeConstraints { maker in
            maker.centerX.equalToSuperview()
            maker.width.equalTo(200)
            maker.bottom.equalToSuperview().offset(-30)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
