//
//  LoginViewController.swift
//  createuifromcode
//
//  Created by HA Duyen Hoa on 21.11.18.
//  Copyright © 2018 HDH. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    override func loadView() {
        let loginView = LoginView()
        loginView.loginButton.addTarget(self,
                                        action: #selector(LoginViewController.login),
                                        for: .touchUpInside)
        loginView.closeButton.addTarget(self,
                                        action: #selector(LoginViewController.closeLoginScreen),
                                        for: .touchUpInside)
        view = loginView
    }

    @objc func login() {
        let alertController = UIAlertController(title: "Information",
                                                message: "Login button clicked",
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }

    @objc func closeLoginScreen() {
        dismiss(animated: true, completion: nil)
    }
}
