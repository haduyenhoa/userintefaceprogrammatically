//
//  UIView+UIComponents.swift
//  createuifromcode
//
//  Created by HA Duyen Hoa on 21.11.18.
//  Copyright © 2018 HDH. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    @objc class func createTextField(_ placeHolder: String? = nil) -> UITextField {
        let textField = UITextField(frame: CGRect.zero)
        textField.layer.borderColor = UIColor.gray.cgColor
        textField.layer.borderWidth = 1.0
        textField.layer.cornerRadius = 5.0
        textField.autocorrectionType = .no
        textField.font = UIFont.systemFont(ofSize: 15)
        textField.tintColor = UIColor.blue
        textField.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)
        textField.autocapitalizationType = .words
        textField.placeholder = placeHolder

        return textField
    }

    @objc class func createButton(_ text: String) -> UIButton {
        let button = UIButton(frame: .zero)
        button.setTitle(text, for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 17)
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.titleLabel?.lineBreakMode = .byClipping
        button.layer.cornerRadius = 6.0
        button.backgroundColor = .blue

        return button
    }
}
